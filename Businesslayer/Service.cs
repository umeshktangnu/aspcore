﻿using Businesslayer.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Xml;

namespace BusinessLayer
{
  public class Service
  {
    private static volatile Service instance;
    private static object syncRoot = new Object();
    private static HttpClient client;

    private Service()
    {
      client = new HttpClient();
      client.BaseAddress = new Uri("https://jazzccm.bt.bombardier.net:9443/ccm/");
      client.DefaultRequestHeaders.Accept.Clear();
      
    }

    public List<ProjectArea> getAllProjectAreas()
    {
      client.DefaultRequestHeaders.Accept.Clear();
      client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/xml"));
      client.DefaultRequestHeaders.Add("OSLC-Core-Version", "2.0");

      HttpResponseMessage response = client.GetAsync("authenticated/identity").Result;
      if (response.IsSuccessStatusCode)
      {
        response = client.PostAsync("authenticated/j_security_check?j_username=utangnu&j_password=Welcome@7", null).Result;
        response = client.GetAsync("rpt/repository/workitem?fields=workitem/projectArea/name").Result;
        if (response.IsSuccessStatusCode)
        {
          var jsonString = response.Content.ReadAsStringAsync();
          XmlDocument doc = new XmlDocument();
          doc.LoadXml(jsonString.Result);
          XmlNodeList nodelist = doc.SelectNodes("/workitem/projectArea/name");
          List<ProjectArea> projects = new List<ProjectArea>();
          foreach (XmlNode node in nodelist) // for each <projectarea> node
          {
            ProjectArea project = new ProjectArea();
            switch (node.NodeType)
            {
              case XmlNodeType.Element:
                if (node.Name == "name")
                {
                  project.Name = node.Name;
                  projects.Add(project);
                }
                break;
              case 0:

                break;
            }

          }
          return projects;
        }
        else
        {
          ;
        }
      }
      else
      {
        ;
      }


      return null;
    }
    public static Service Instance
    {
      get
      {
        if (instance == null)
        {
          lock (syncRoot)
          {
            if (instance == null)
              instance = new Service();
          }
        }

        return instance;
      }
    }
  }
}
