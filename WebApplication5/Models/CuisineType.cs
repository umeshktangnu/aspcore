﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication5.Models
{
  public enum CuisineType
  {
    None,
    Italian,
    French,
    Indian,
    German
  }
}
