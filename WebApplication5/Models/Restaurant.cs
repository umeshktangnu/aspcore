﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
namespace WebApplication5.Models
{
  public class Restaurant
  {
    [Display(Name="Hotel Ka Naame")]
    [DataType(DataType.Text)]
    [Required]
    public string Name { get; set; }
    public int ID { get; set; }

    [Required]
    public CuisineType Cuisine { get; set; }
  }
}
