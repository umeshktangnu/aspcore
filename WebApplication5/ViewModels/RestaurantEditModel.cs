﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WebApplication5.Models;

namespace WebApplication5.ViewModels
{
  public class RestaurantEditModel
  {
    [DataType(DataType.Text)]
    [Required]
    public string Name { get; set; }

    [Required]
    public CuisineType Cuisine { get; set; }

  }
}
