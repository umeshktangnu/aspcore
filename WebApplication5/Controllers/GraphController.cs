﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication5.Models;

namespace WebApplication5.Controllers
{
  public class GraphController : Controller
  {
    public IActionResult Canvas()
    {
      double count = 1000, y = 100;
      Random random = new Random();
      List<DataPoint> dataPoints = new List<DataPoint>();

      for (int i = 0; i < count; i++)
      {
        y += random.Next(-10, 11);
        dataPoints.Add(new DataPoint(i, y));
      }

      ViewBag.DataPoints = JsonConvert.SerializeObject(dataPoints);
      return View();
    }

    public IActionResult DevExpress()
    {
      return View();
    }

    public IActionResult Google()
    {
      return View();
    }

    public IActionResult Telerik()
    {
      return View();
    }
  }
}
