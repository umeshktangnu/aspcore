﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using WebApplication5.Models;
using WebApplication5.Services;
using WebApplication5.ViewModels;

namespace WebApplication5.Controllers
{
  
  
  public class HomeController :Controller
  {
    private IRestaurantData _restaurantData;
    private IGreeter _greeter;

    public HomeController(IRestaurantData  restaurantData,IGreeter greeter)
    {
      _restaurantData = restaurantData;
      _greeter = greeter;
    }
    public string Index(int id)
    {
      return $"No Restaurant was found with id= {id}";
    }

    
    public string test ()
    {
      return "Awesome";
    }
    public IActionResult Restaurant()
    {
      var model = new HomeRestaurantViewModel();
      model.Restaurants = _restaurantData.GetAll();
      model.CurrentMessage = (_greeter as Greeter).GREETING;
      return View(model);
    }
    public IActionResult Result()
    {
      var msg =new Greeter(null);
      return View(msg);
    }
    public IActionResult Details(int id)
    {
      var model = _restaurantData.Get(id);
      if (model == null)
      {
        return RedirectToAction(nameof(Index),id);
      }
      return View(model);
      
    }

    [HttpGet]
    public IActionResult Create()
    {
      return View();
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public IActionResult Create(RestaurantEditModel model )
    {
      if (ModelState.IsValid)
      { 
      Restaurant restaurant = new Restaurant();
      restaurant.Name = model.Name;
      restaurant.Cuisine = model.Cuisine;
      _restaurantData.Add(restaurant);
      return RedirectToAction(nameof(Details), new {id=restaurant.ID });
      }
      else
      {
        return View();
      }
    }
  }
}
