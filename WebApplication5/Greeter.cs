﻿using Microsoft.Extensions.Configuration;
using WebApplication5.Services;

namespace WebApplication5
{
  public class Greeter : IGreeter
  {
    private IConfiguration _configuration;

    public Greeter(IConfiguration configuration)
    {
      _configuration = configuration;
    }
    public string getGreeting()
    {

      return _configuration["jazz_user"];
    }
    public string GREETING { get => "jazz_user"; }
  }
}