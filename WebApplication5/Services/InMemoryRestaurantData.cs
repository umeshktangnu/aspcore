﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication5.Models;

namespace WebApplication5.Services
{
  public class InMemoryRestaurantData:IRestaurantData
  {
    List<Restaurant> _restaurant;

    public InMemoryRestaurantData()
    {
      _restaurant = new List<Restaurant>()
      {
        new Restaurant{ID=1,Name="KFC"},
        new Restaurant{ID=2,Name="MCD"},
        new Restaurant{ID=3,Name="Dominos"}
      };
    }

    public void Add(Restaurant restaurant)
    {
      restaurant.ID = _restaurant.Max(r => r.ID) + 1;
      _restaurant.Add(restaurant);
    }

    public Restaurant Get(int id)
    {
      return _restaurant.FirstOrDefault(r => r.ID == id);
    }

    public IEnumerable<Restaurant> GetAll()
    {
      return _restaurant.OrderBy(r => r.Name);
    }
  }
}
