﻿namespace WebApplication5.Services
{
  public interface IGreeter
  {
    string getGreeting();
  }
}