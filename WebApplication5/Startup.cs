﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using WebApplication5.Services;

namespace WebApplication5
{
  public class Startup
  {
    // This method gets called by the runtime. Use this method to add services to the container.
    // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
    public void ConfigureServices(IServiceCollection services)
    {
      services.AddSingleton<IGreeter, Greeter>();
      services.AddSingleton<IRestaurantData, InMemoryRestaurantData>();
      services.AddMvc();
    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app,
      IHostingEnvironment env,
      IGreeter greeter,
      ILogger<Startup> logger)
    {
      if (env.IsDevelopment())
      {
        app.UseDeveloperExceptionPage();
      }
      //app.UseDefaultFiles();
      app.UseStaticFiles();
      app.UseMvc(configureRoutes);
      #region test
      //app.Use(next =>
      //{
      //  return async context =>
      //  {
      //    logger.LogInformation("Request incoming");
      //    if (context.Request.Path.StartsWithSegments("/aba"))
      //    {
      //      await context.Response.WriteAsync("Hit!!");
      //      logger.LogInformation("Request Handeled");
      //    }
      //    else
      //    {
      //      await next(context);
      //      logger.LogInformation("Request outgoing");
      //    }
      //  };
      //});
      //app.UseWelcomePage(new WelcomePageOptions
      //{
      //  Path = "/test"
      //});
      //
      //app.Run(async (context) =>
      //{

      //  await context.Response.WriteAsync($"Hello ! + {greeter.getGreeting()}");
      //});
      #endregion
    }

    private void configureRoutes(IRouteBuilder routeBuilder)
    {
      routeBuilder.MapRoute("Default", "{controller}/{Action}/{id?}");
    }
  }
}
